import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  Column buttonSection(Color buttonColor, IconData icon, String label) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Align(alignment: Alignment.centerLeft,),
        SizedBox(
          width: 75,
          height: 130,
          child: Container(
            color: Colors.grey,
            child: Column(
              children: [
                Align(alignment: Alignment.bottomCenter,),
                Icon(icon, color: buttonColor, size: 50),
                Text(label),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget build(BuildContext context) {
    Widget buttonColumn = Container(
      child: Column(
        children: [
          buttonSection(Colors.lightBlue, Icons.whatshot_outlined, 'Cook'),
          buttonSection(Colors.lightBlue, Icons.wysiwyg_outlined, 'Recipes'),
          buttonSection(Colors.lightBlue, Icons.add_a_photo_outlined, 'Pantry'),
        ],
      ),
    );

    return MaterialApp(
        title: 'Scrapbook',
        theme: ThemeData(
          primarySwatch: Colors.teal,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('481 HW2: Scrapbooking'),
          ),
          body: ListView(
            children: [
              Image.asset('images/Layout Idea.png'),
              buttonColumn,
            ],
          ),
        ));
  }
}
